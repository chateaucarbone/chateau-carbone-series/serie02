// monstershield MEGA

#define PWM1 5
#define CW1 7 
#define CCW1 8

int del = 100;
int value = 0;
int pwm_min = 15;
int pwm_max = 29;

void setup()
{
    pinMode(PWM1, OUTPUT);
    pinMode(CW1, OUTPUT);
    pinMode(CCW1, OUTPUT);
    digitalWrite(CW1, LOW);
    digitalWrite(CCW1, HIGH);  

  // ------------------ OFF
  analogWrite(PWM1, 0);
  delay(60000);
}

void loop()
{
  // ----------------- ON
  del = random(100,300);
 // pwm_max = (int) random(50, 60);

  // acceleration
  for(value = pwm_min ; value <= pwm_max; value+=1)
  {
    analogWrite(PWM1, value);
    delay(del);
  }

  // vitesse constante durée
  int t = del * (pwm_max - pwm_min);
  delay(180000 - (2 * t)); 
  
  // desceleration
  for(value = pwm_max ; value >= pwm_min; value-=1)
  {
    analogWrite(PWM1, value);
    delay(del);
  }



  // ------------------ OFF
  analogWrite(PWM1, 0);
  delay(60000);
}
